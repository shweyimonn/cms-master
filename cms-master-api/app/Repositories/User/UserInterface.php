<?php

namespace App\Repositories\User;

interface UserInterface 
{
    public function register($request);

    public function login($request);

    public function getUsers();

    public function getProfile();

    public function updateProfile($request);

    public function delete();

    public function logout();
}