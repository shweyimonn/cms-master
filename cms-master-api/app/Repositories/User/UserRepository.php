<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Repositories\User\UserInterface;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserInterface
{
    protected $user = null;
    public function register($request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return $user;
    }

    public function login($request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return ['success' => false, 'status' => 401, 'message' => 'Invalid Credentials.'];
        }
        $user = User::where('email', $request['email'])->firstOrFail();
        $token = $user->createToken('auth_token')->plainTextToken;

        return ['user' => $user, 'token' => $token, 'success' => true, 'status' => 200];
    }

    public function getUsers()
    {
        return User::all();
    }

    public function getProfile()
    {
        return auth()->user();
    }

    public function updateProfile($request)
    {
        $id = auth()->user()->id;
        $user = User::find($id);
        if (isset($request['password'])) {
            $request['password'] = Hash::make($request['password']);
        }
        $user->update($request);
        return $user;
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
    }

    public function delete()
    {
        $id = auth()->user()->id;
        return User::find($id)->delete();
    }
}
