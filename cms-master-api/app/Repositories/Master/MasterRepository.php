<?php

namespace App\Repositories\Master;

use App\Models\Master;
use App\Repositories\Master\MasterInterface;

class MasterRepository implements MasterInterface
{
    public function getMasters()
    {
        return Master::all();
    }

    public function getMasterById($id)
    {
        return Master::find($id);
    }
}