<?php

namespace App\Repositories\Master;

interface MasterInterface 
{
    public function getMasters();

    public function getMasterById($id);
}