<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Master\MasterInterface;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public $master;

    public function __construct(MasterInterface $master)
    {
        $this->master = $master;
    }

    public function index()
    {
        $masters = $this->master->getMasters();
        return response()->json([
            "success" => true,
            "masters" => $masters
        ]);
    }

    public function show($id)
    {
        $master = $this->master->getMasterById($id);
        return response()->json([
            "success" => true,
            "master" => $master
        ]);
    }
}
