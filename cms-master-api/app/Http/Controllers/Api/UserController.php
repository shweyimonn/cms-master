<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * to create brand new user
     */
    public function register(Request $request)
    {
        $field = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email|email',
            'password' => 'required|string|min:8|max:30'
        ]);
        $user = $this->user->register($field);

        return response()->json([
            'success' => true,
            'message' => 'Brand new user ' . $user['name'] . ' is created.',
            'user' => $user
        ]);
    }

    /**
     * to login account
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:8|max:30'
        ]);
        $data = $this->user->login($request);
        if ($data['success'] !== true) {
            return response()->json([
                'success' => $data['success'],
                'message' => $data['message']
            ], $data['status']);
        }

        return response()->json([
            'success' => $data['success'],
            'user' => $data['user'],
            'token' => 'Bearer ' . $data['token'],
        ]);
    }

    /**
     * get profile
     */
    public function show()
    {
        $user = $this->user->getProfile();
        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    /**
     * get all users
     */
    public function index()
    {
        $users = $this->user->getUsers();
        return response()->json([
            'success' => true,
            'users' => $users
        ]);
    }

    /**
     * edit users
     */
    public function edit(Request $request)
    {
        $field = $request->validate([
            'name' => 'string',
            'email' => 'string|unique:users,email|email',
            'password' => 'string|min:8|max:30'
        ]);
        $user = $this->user->updateProfile($field);
        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    /**
     * to logout account
     */
    public function logout()
    {
        $this->user->logout();
        return response()->json([
            'success' => true,
            'message' => 'User successfully logged out.'
        ]);
    }

    // delete my account
    public function delete()
    {
        $this->user->delete();
        return response()->json([
            "success" => true,
            "message" => "Deleted user successfully."
        ]);
    }
}
