## CMS MASTER
 RESTFUL API server of CMS

## Requirements, Frameworks and tools
1. [laravel v8.6.10](https://laravel.com/)
2. [PHP v7.3.28](https://www.php.net/)
3. [Insomnia](https://insomnia.rest/download)

### Prepare for seeding
- create `.env` file and copy from `.env.example`
- modify `database` name, `database user` name and `database password` for your os.

### Install Library, packages
```bash
$ composer update
```

### Create Tables (migration)
```bash
$ php artisan migrate
```

### Insert Master Data (seeding)
```bash
$ php artisan db:seed
```

### To Run API server
```bash
$ php artisan serve
```

### Docker setup
#### To build docker image
```bash
$ docker compose build
```

### To run dashboard with docker
```bash
$ docker compose up -d
```

### To Test API in RESTFUL API Tool (insomina)
1. Download [Insomnia](https://insomnia.rest/download)
2. Open Insomnia

<img src="https://user-images.githubusercontent.com/32084431/150601114-2723ea65-a773-4f1a-8823-2d0c417cf44e.png" width="70%" height="70%" />

3. Import `api.json` to add collections in it
   1. Click Insomnia
   2. Click Import/Export

<img src="https://user-images.githubusercontent.com/32084431/150601315-a84cb99d-f24e-4ef9-a95a-506a0ee55005.png" width="70%" height="70%" />

   3. Click Import Data
   4. Choose `From File`

<img src="https://user-images.githubusercontent.com/32084431/150601750-a07491a3-57e5-4bee-9564-01d6a153913b.png" width="70%" height="70%" />
    
    5. Import `api.json` file

<img src="https://user-images.githubusercontent.com/32084431/150601964-ed9943a5-0371-48c6-a296-0fcb193e5698.png" width="70%" height="70%" />

<img src="https://user-images.githubusercontent.com/32084431/150602123-e95f1fc6-4a07-4fb8-9a18-166c661e32fe.png" width="70%" height="70%" />

4. Can send HTTP request to server like the following

<img src="https://user-images.githubusercontent.com/32084431/150602455-13cf8abe-addd-447e-a919-bfc22738a3a0.png" width="70%" height="70%" />
