<?php

use App\Http\Controllers\Api\MasterController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// register brand new user
Route::post('register', [UserController::class, 'register']);
// login account
Route::post('login', [UserController::class, 'login']);
// sanctum auth group
Route::group(['middleware' => ['auth:sanctum']], function () {
    // get master list
    Route::get('masters', [MasterController::class, 'index']);
    // get master by id
    Route::get('masters/{id}', [MasterController::class, 'show']);
    // get user list
    Route::get('users', [UserController::class, 'index'])->name('user.list');
    // get profile
    Route::get('me', [UserController::class, 'show'])->name('user.me');
    // update profile
    Route::put('profile', [UserController::class, 'edit'])->name('user.edit');
    // delete account
    Route::delete('profile', [UserController::class, 'delete'])->name('user.delete');
    // logout account
    Route::post('logout', [UserController::class, 'logout']);
});
