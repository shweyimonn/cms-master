### Docker setup
#### To build docker image
```bash
$ docker compose build
```

### To run dashboard with docker
```bash
$ docker compose up -d
```