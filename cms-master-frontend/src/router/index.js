import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Master
const MasterDetail = () => import('@/views/Master/MasterDetail')
const MasterList = () => import('@/views/Master/MasterList')

// User
const UserList = () => import('@/views/User/UserList')
const UserProfile = () => import('@/views/User/UserProfile')

const Login = () => import('@/views/Login')
const Signup = () => import('@/views/Signup')
const Page404 = () => import('@/views/Page404')

Vue.use(Router)

export const router = new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/master/list',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'master',
          name: 'Master',
          redirect: "/master/list",
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "list",
              name: "MasterList",
              component: MasterList,
              meta: {
                isAuthed: true,
              },
            },
            {
              path: "detail/:id",
              name: "MasterDetail",
              component: MasterDetail,
              meta: {
                isAuthed: true,
              },
            },
          ]
        },
        {
          path: 'user',
          name: 'User',
          redirect: "/user/list",
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "list",
              name: "UserList",
              component: UserList,
              meta: {
                isAuthed: true,
              },
            },
            {
              path: "profile",
              name: "UserProfile",
              component: UserProfile,
              meta: {
                isAuthed: true,
              },
            },
          ]
        }
      ]
    },

    { path: "*", redirect: "/404" },

    {
      path: "/",
      redirect: "/",
      name: "Auth",
      component: {
        render(c) {
          return c("router-view");
        },
      },
      children: [
        {
          path: "login",
          name: "login",
          component: Login,
          meta: {
            isAuthed: false,
          },
        },
        {
          path: "signup",
          name: "signup",
          component: Signup,
          meta: {
            isAuthed: false,
          },
        },
        {
          path: "404",
          name: "page404",
          component: Page404,
          meta: {
            isAuthed: true,
          },
        },
      ]
    }
  ]
})

/**
 * Route Authentication beforeEach
 */
 router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const token = localStorage.getItem("accessToken");
  if (to.meta.isAuthed) {
    if (!token) {
      next("/login")
    } else {
      next()
    }
  } else {
    if(token) {
      next("/master/list")
    } else {
      next()
    }
  }
});
