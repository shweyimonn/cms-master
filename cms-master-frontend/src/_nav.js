export default {
  items: [
    {
      name: "User",
      url: "/user",
      icon: "icon-user",
      children: [
        {
          name: "Profile",
          url: "/user/profile",
          icon: "icon-user",
        },
        {
          name: "List",
          url: "/user/list",
          icon: "icon-cursor",
        },
      ],
    },
    {
      name: "Master",
      url: "/master",
      icon: "icon-star",
      children: [
        {
          name: "MasterList",
          url: "/master/list",
          icon: "icon-cursor",
        },
      ],
    },
  ],
};
