import Axios from "axios";

export const authService = {
  signup,
  login,
  logout,
};

async function signup(params) {
  const result = Axios({
    method: "POST",
    url: `${process.env.VUE_APP_ENDPOINT}/register`,
    headers: {
      "content-type": "application/json",
    },
    data: JSON.stringify(params),
  });
  return result;
}

async function login(params) {
  const result = Axios({
    method: "POST",
    url: `${process.env.VUE_APP_ENDPOINT}/login`,
    headers: {
      "content-type": "application/json",
    },
    data: JSON.stringify(params),
  });
  return result;
}

async function logout() {
  const result = Axios({
    method: "POST",
    url: `${process.env.VUE_APP_ENDPOINT}/logout`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
  });
  return result;
}
