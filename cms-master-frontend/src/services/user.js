import Axios from "axios";

export const userService = {
  getProfile,
  profileUpdate,
  getUsers,
};

async function getProfile() {
  const result = Axios({
    method: "GET",
    url: `${process.env.VUE_APP_ENDPOINT}/me`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
  });
  return result;
}

async function profileUpdate(params) {
  const result = Axios({
    method: "PUT",
    url: `${process.env.VUE_APP_ENDPOINT}/profile`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
    data: JSON.stringify(params),
  });
  return result;
}

async function getUsers() {
  const result = Axios({
    method: "GET",
    url: `${process.env.VUE_APP_ENDPOINT}/users`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
  });
  return result;
}
