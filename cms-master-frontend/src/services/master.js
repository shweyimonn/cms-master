import Axios from "axios";

export const masterService = {
  getMasters,
  getMaster,
};

async function getMasters() {
  const result = Axios({
    method: "GET",
    url: `${process.env.VUE_APP_ENDPOINT}/masters`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
  });
  return result;
}

async function getMaster(id) {
  const result = Axios({
    method: "GET",
    url: `${process.env.VUE_APP_ENDPOINT}/masters/${id}`,
    headers: {
      "content-type": "application/json",
      Authorization: localStorage.getItem("accessToken"),
    },
  });
  return result;
}
