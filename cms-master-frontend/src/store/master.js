import { masterService } from "../services";

const state = {
  masters: [],
  master: {},
  error_message: null,
  success_message: null,
  loading: false,
};

const getters = {
  masters: (state) => state.masters,
  master: (state) => state.master,
  error_message: (state) => state.error_message,
  success_message: (state) => state.success_message,
  loading: (state) => state.loading,
};

const actions = {
  getMasters({ commit }) {
    commit("resetMessage");
    state.loading = true;
    masterService
      .getMasters()
      .then((res) => {
        commit("setMasters", res.data.masters);
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  },

  getMaster({ commit }, params) {
    commit("resetMessage");
    state.loading = true;
    masterService
      .getMaster(params)
      .then((res) => {
        commit("setMaster", res.data.master);
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  }
};

const mutations = {
  setMasters(state, params) {
    state.masters = params;
    state.error_message = null;
    state.success_message = null;
    state.loading = false;
  },

  setMaster(state, params) {
    state.master = params;
    state.error_message = null;
    state.success_message = null;
    state.loading = false;
  },

  setErrorMessage(state, params) {
    state.error_message = params.message;
    state.success_message = null;
    state.loading = false;
  },

  setSuccessMessage(state, params) {
    state.error_message = null;
    state.success_message = params.message;
    state.loading = false;
  },

  resetMessage(state) {
    state.error_message = null;
    state.success_message = null;
  },
};

export const master = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
