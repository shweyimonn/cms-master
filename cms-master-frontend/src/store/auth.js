import { authService } from "../services";
import { router } from "../router";

const state = {
  error_message: null,
  success_message: null,
  loading: false,
};

const getters = {
  error_message: (state) => state.error_message,
  success_message: (state) => state.success_message,
  loading: (state) => state.loading,
};

const actions = {
  login({ commit }, params) {
    commit("resetMessage");
    state.loading = true;
    authService
      .login(params)
      .then((res) => {
        localStorage.setItem('accessToken', res.data.token)
        const user = JSON.stringify(res.data.user)
        localStorage.setItem('user', user)
        commit("setSuccessMessage", "");
        router.push("/master/list")
      })
      .catch((err) => {
        if (err.response) {
          commit("setErrorMessage", err.response.data);
        }
      });
  },

  signup({ commit }, params) {
    commit("resetMessage");
    state.loading = true;
    authService
      .signup(params)
      .then((res) => {
        commit("setSuccessMessage", res.data);
        setTimeout(() => {
          router.push("/login");
        }, 100);
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  },

  logout() {
    authService
      .logout()
      .then(() => {
        localStorage.clear();
        router.push("/login");
      })
      .catch(() => {
        localStorage.clear();
        router.push("/login");
      });
  },
};

const mutations = {
  setErrorMessage(state, params) {
    state.error_message = params.message;
    state.success_message = null;
    state.loading = false;
  },

  setSuccessMessage(state, params) {
    state.error_message = null;
    state.success_message = params.message;
    state.loading = false;
  },

  resetMessage(state) {
    state.error_message = null;
    state.success_message = null;
  },
};

export const auth = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
