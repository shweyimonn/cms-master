import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './auth'
import { user } from './user'
import { master } from './master'

Vue.use(Vuex)

/**
 *eslint-disable no-new
 */
export const store = new Vuex.Store({
  modules: {
    auth,
    user,
    master,
  },
})
