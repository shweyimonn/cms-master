import { userService } from "../services";
import { router } from "../router";

const state = {
  users: [],
  user: {},
  error_message: null,
  success_message: null,
  loading: false,
};

const getters = {
  users: (state) => state.users,
  user: (state) => state.user,
  error_message: (state) => state.error_message,
  success_message: (state) => state.success_message,
  loading: (state) => state.loading,
};

const actions = {
  getUsers({ commit }) {
    commit("resetMessage");
    state.loading = true;
    userService
      .getUsers()
      .then((res) => {
        commit("setUsers", res.data.users);
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  },

  getProfile({ commit }) {
    commit("resetMessage");
    state.loading = true;
    userService
      .getProfile()
      .then((res) => {
        commit("setUser", res.data.user);
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  },

  updateProfile({ commit }, params) {
    commit("resetMessage");
    state.loading = true;
    userService
      .profileUpdate(params)
      .then((res) => {
        const user = JSON.stringify(res.data.user)
        localStorage.removeItem('user')
        localStorage.setItem('user', user)
        commit("setUser", res.data.user);
        router.push('/user/list')
      })
      .catch((err) => {
        commit("setErrorMessage", err.response.data);
      });
  },
};

const mutations = {
  setUsers(state, params) {
    state.users = params;
    state.error_message = null;
    state.success_message = null;
    state.loading = false;
  },

  setUser(state, params) {
    state.user = params;
    state.error_message = null;
    state.success_message = null;
    state.loading = false;
  },

  setErrorMessage(state, params) {
    state.error_message = params.message;
    state.success_message = null;
    state.loading = false;
  },

  setSuccessMessage(state, params) {
    state.error_message = null;
    state.success_message = params.message;
    state.loading = false;
  },

  resetMessage(state) {
    state.error_message = null;
    state.success_message = null;
  },
};

export const user = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
