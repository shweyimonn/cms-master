## CMS MASTER
 Frontend Website of CMS Master App


## Installation
### Usage

``` bash
# install app's dependencies
$ npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# run linter
npm run lint
